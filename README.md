# Data platform

## Tools

- Data platform (ckan): http://dataplatform-proto.ugent.be (Issue #9)
- GeoViz: http://geoviz-proto.ugent.be
- Jorge viz: http://movetest.ugent.be:3000/ (Issue #6)

## Dataset Fietstelweek 2016

- **Project:**    Fietstelweek 2016
- **Period:**     2016-09-19 - 2016-09-26 23:45:00
- **Type:**       Bike counting data
- **Records:**    381195 obs. of 11 variables
- **Source:**     Counting data
- **Date:**       Jun 27, 2017
- **Author:**     Angel J. Lopez @ UGent
- **Datafile:** [fietstelweek2016.20170127.csv.gz](data/fietstelweek2016.20170127.csv.gz)

### Fields

- time_from:  Start time of the counting
- time_to:    End time of the counting
- value:      Number of vehicles (bicycles)
- site:       Site name
- source:     Datasource (GeoViz or Manual)
- name:       Province name (OSM Administrative level 6)
- X:          Longitude (degress)
- Y:          Latitude (degrees)
- hour:       Time label


![mobile_rcunnamed-chunk-5-1](data/c4e826dc-e3d8-11e6-98eb-f514a81ec46f.png)


## Received data 

Original files received from Fietsberaad:

- Oct. #4: [FTW2016_UGent_teldata_20161026.zip](data/FTW2016_UGent_teldata_20161026.zip) 
- Nov. #8: [FTW2016_Ugent_teldata_20161124.zip](data/FTW2016_Ugent_teldata_20161124.zip)
- Dec. #8: [FTW2016_UGent_teldata_20161201.zip](data/FTW2016_UGent_teldata_20161201.zip)
- Jan. #7: City of Ghent (7 counting points)
    + [20161107_DO_Fietstelweek_HTSPLITS_2016.xlsx](data/Ghent/20161107_DO_Fietstelweek_HTSPLITS_2016.xlsx)
    + [20161107_DO_Fietstelweek.xlsx](data/Ghent/20161107_DO_Fietstelweek.xlsx)
    + Location site (image + street name) [20160914_DO_Locaties telslangen fietstelweek_2.pdf](data/Ghent/20160914_DO_Locaties.telslangen.fietstelweek_2.pdf)

### Processed files status

- Fully processed [FTW2016_UGent_teldata_20161026.status.xlsx](data/FTW2016_UGent_teldata_20161026.status.xlsx)
- Partly processed [FTW2016_UGent_teldata_20161124.status.xlsx](data/FTW2016_UGent_teldata_20161124.status.xlsx)
- Partly processed [FTW2016_UGent_teldata_20161201.status.xlsx](data/FTW2016_UGent_teldata_20161201.status.xlsx)
- Fully processed: City of Ghent


### Fietsberaad

(Dominique)
These are the number which I received from Fietsberaad:
-        Fixed count locations:    14

(permanent locations, counting 24/7 during all the year. We probably received data for a long period
-        Counting devices:            144

(counting tubes installed especially fort he Bike Count Week, probably for one or two weeks)
-        Manual counts:                145
(manual counting, only for a short period of a few hours on one or maybe two days)


# Data platform Fiets

- Requirements (google doc): [Dataplatform fiets - requirements](https://docs.google.com/spreadsheets/d/1m3QIVuHwKcRUhsGHjmjh8oAEKV6qYaAZHJyxqHJudio/edit?usp=sharing_eidl&invite=CNn21PIK&ts=580e236c)

![](figs/homepage-original.png)

http://dataplatform.ugent.be/
